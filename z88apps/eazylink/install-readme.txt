EazyLink Standalone 5.2.4
-------------------------

This version of EazyLink was superseded by the versions built into OZ from version 4.4 onwards. Though it is
limited to 9600bps, it remains useful for ensuring reliable serial file transfers to and from Z88 machines
running earlier versions of OZ.


How to install EazyLink popdown on your Z88
-------------------------------------------
 
1) Get latest RomUpdate from Sourceforge: https://sourceforge.net/projects/z88/files/Z88%20Applications/
2) Unzip the RomUpdate archive.
3) From the BBC BASIC folder within, transfer "romupdate.bas" to your Z88.
   Make sure that the files are transfered binary intact (no byte translation or linefeed conversion)

4) Transfer "romupdate.cfg" and "eazylink.63" files to your Z88 (that were part of this ZIP file)
   Again make sure that these files are transfered binary intact (no byte translation or linefeed conversion).

5) On your Z88, create a BBC BASIC application instance, #B
6) type: RUN"ROMUPDATE.BAS"

7) Confirm the slot number of where your blank card has been inserted (typically in slot 3)

RomUpdate will now write the popdown code to the card.
Go to index, and re-insert card. EazyLink popdown is now available. Type []L to start it..

If you want to merge Eazylink with existing applications on a card, you have to use RomCombiner to copy those
first, then combine / add the EazyLink popdown code bank to that list, and finally re-blow
all banks to a blank Flash or UV Eprom card.


EazyLink project wiki on Internet
-------------------------------------------
https://cambridgez88.jira.com/wiki/x/AgBN

Here you will find latest news about the popdown.

Latest news about Z88 project:
https://cambridgez88.jira.com
