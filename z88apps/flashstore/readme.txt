-------------------------------------------------------------------------------------
FlashStore compilation notes
-------------------------------------------------------------------------------------

To compile the FlashStore application, execute the following:

1) Select the directory holding the FlashStore files as the current directory.
2) Execute:
                makeapp.bat (DOS/Windows command line script)
                makeapp.sh  (UNIX/LINUX shell script)

   This will create the executable file "fsapp.bin", "romhdr.bin" and create
   the "flashstore.epr" card file.

-------------------------------------------------------------------------------------

If you need to compile a previous release, you should clone another branch from the
git repository.

The latest development release (master branch) can be found here:
        https://bitbucket.org/cambridge/z88/src/master/z88apps/flashstore/
